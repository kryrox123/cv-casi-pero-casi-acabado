import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactosComponent } from './components/contactos/contactos.component';
import { DatosComponent } from './components/datos/datos.component';
import { MiPersonaComponent } from './components/mi-persona/mi-persona.component';

const routes: Routes = [
  {path: 'mi-persona', component:MiPersonaComponent },
  {path: 'datos', component:DatosComponent },
  {path: 'contactos', component:ContactosComponent },
  {path: '**', pathMatch:'full', redirectTo: 'mi-persona'}

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
