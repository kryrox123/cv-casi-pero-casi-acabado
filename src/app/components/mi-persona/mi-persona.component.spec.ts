import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiPersonaComponent } from './mi-persona.component';

describe('MiPersonaComponent', () => {
  let component: MiPersonaComponent;
  let fixture: ComponentFixture<MiPersonaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MiPersonaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MiPersonaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
