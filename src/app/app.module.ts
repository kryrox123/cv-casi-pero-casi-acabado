import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MiPersonaComponent } from './components/mi-persona/mi-persona.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DatosComponent } from './components/datos/datos.component';
import { ContactosComponent } from './components/contactos/contactos.component';
import { AgendaElectronicaComponent } from './components/agenda-electronica/agenda-electronica.component';

@NgModule({
  declarations: [
    AppComponent,
    MiPersonaComponent,
    NavbarComponent,
    DatosComponent,
    ContactosComponent,
    AgendaElectronicaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
